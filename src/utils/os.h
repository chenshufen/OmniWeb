#ifndef     __OMNI_OS_H_INCLUDED__
#define     __OMNI_OS_H_INCLUDED__

#include    <cstdlib>

/** Check does a path/file exist */
extern bool Exists(const char * path);

/** Make directory */
extern bool MakeDir(const char * path);

/** Copy file */
extern bool Copy(const char * path, const char * to);

/** Delete file */
extern bool Delete(const char * path);

/** Get file size */
extern size_t FileSize(const char * path);

#endif//!   __OMNI_OS_H_INCLUDED__