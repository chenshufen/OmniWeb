#ifndef     __OMNI_CRYPTO_H_INCLUDED__
#define     __OMNI_CRYPTO_H_INCLUDED__

#include    <cstdint>
#include    <string>

/**
 * Calculate CRC32
 * 
 * \param   mem     Memory contains data to be calculated
 * \param   size    Size of data in bytes.
 * \param   prev    Appened CRC32 result for streaming way.
 * \return CRC result.
 */
extern uint32_t CRC32(const char * mem, size_t size, uint32_t prev = 0);

/**
 * Hash data via BKDR method
 * 
 * \param   mem     Memory contains data to be calculated
 * \param   size    Size of data in bytes.
 * \return Hash result.
 */
extern uint32_t BKDRHash(const char * mem, size_t size);

/**
 * Get MD5 result of data
 * 
 * \param   mem     Memory contains data to be calculated
 * \param   size    Size of data in bytes.
 * \result MD5 hex string.
 */
extern std::string MD5(const char * mem, size_t size);

/**
 * Encode string with Base64
 * 
 * \param   mem     Memory contains data to be encoded
 * \param   size    Size of data in bytes.
 * \return Base64 result string
 */
extern std::string Base64Encode(const char * mem, size_t size);

/**
 * Decode Base64 encoded string
 * 
 * \param   mem     Memory contains encoded string.
 * \param   size    Size of data in bytes.
 * \return Original string
 */
extern std::string Base64Decode(const char * mem, size_t size);

#endif//!   __OMNI_CRYPTO_H_INCLUDED__