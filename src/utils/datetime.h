#ifndef     __OMNI_DATETIME_H_INCLUDED__
#define     __OMNI_DATETIME_H_INCLUDED__

#include    <string>

/**
 * Date Time
 */
struct DateTime {
    int year;
    int month;
    int day;
    int hour;
    int minute;
    int second;
    int millisec;

    DateTime();
    DateTime(int y, int m, int d, int h, int min, int s)
        : year(y), month(m), day(d), hour(h), minute(min), second(s), millisec(0) {}

    int Timestamp() const;
};

/** Diff time in milliseconds **/
extern double operator-(const DateTime & l, const DateTime & r);

/** Get high resolution CPU time in milliseconds */
extern double Tick();

/** Format time to HTTP GM time */
extern std::string GMTime(time_t timestamp);

#endif//!   __OMNI_DATETIME_H_INCLUDED__