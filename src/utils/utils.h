#ifndef     __OMNI_UTILS_H_INCLUDED__
#define     __OMNI_UTILS_H_INCLUDED__

#include    <string>
#include    <vector>

/** Create UUID/GUID string */
extern std::string CreateId();

/** Split string */
extern std::vector<std::string> Split(const std::string & s, const std::string & delim, bool ignore_empty = true);

#endif//!   __OMNI_UTILS_H_INCLUDED__