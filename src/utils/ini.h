#ifndef     __OMNI_INI_H_INCLUDED__
#define     __OMNI_INI_H_INCLUDED__

#include    <map>
#include    <string>
#include    <vector>

/**
 * Reading INI configuration.
 */
class Ini {
public:
	typedef std::map<std::string, std::map<std::string, std::string>>	Sessions;

public:
	Ini() {}

	/**
	 * Load an INI file in gaven path.
	 *
	 * \param	path	Path to find this file.
	 * \return	Is this file successfully loaded. You can use GetError() to get the detail error.
	 */
	bool Load(const std::string & path);

	/**
	 * If this INI file failed to load, this will return the detail information.
	 *
	 * \return	Detail information for failure.
	 */
	std::string	GetError();

	/**
	 * Get all sessions' name in this INI file.
	 *
	 * \return	All sessions' name in a vector.
	 */
	std::vector<std::string> GetSessions();

	/**
	 * Get all keys in a session.
	 * 
	 * \param	session		Name of session
	 * \return	All keys belong to this session.
	 */
	std::vector<std::string> GetKeys(const std::string & session);

	/**
	 * Get value of special configuration.
	 *
	 * \param	session	Session name of this configuration.
	 * \param	key		Key of this configuration.
	 * \param	def		Default value for this configuration.
	 * \return	Value of this configuration. If the configuration does NOT exists, returns the default value.
	 */
	std::string Get(const std::string & session, const std::string & key, const std::string & def) const;

	/**
	 * Push this configuration to lua layer as table on top of current stack
	 * 
	 * \param	lua		Lua VM
	 */
	void Push2Lua(struct lua_State * lua);

private:
	Sessions	_sessions;
	int			_error;
};
#endif//!   __OMNI_INI_H_INCLUDED__