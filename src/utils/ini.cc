#include    "ini.h"

#include	<cstdio>
#include	<cstring>

extern "C" {
#include    <luajit.h>
#include    <lauxlib.h>
#include    <lualib.h>
}

namespace {
	enum State {
		Success = 0,
		Param,
		FileAccess,
		LostSession,
		LostKey,
		SessionSyntax,
		KeySyntax,
		ValueSyntax,
		BraceMismatch,
		QuoteMismatch,
		EqualMismatch,
	};

    static std::string __INITRIM(const char * p, const std::string & t) {
        std::string r = p;
        std::string::size_type start = r.find_first_not_of(t);
        std::string::size_type end = r.find_last_not_of(t);

        if (start == std::string::npos) r = "";
        else r = r.substr(start, end - start + 1);

        return std::move(r);
    }

	static char * __INISKIP(char * p) {
		if (!p || !(*p)) return p;
		
		char * r = p;
		char c = *r;

		while (c && (c == ' ' || c == '\r' || c == '\n' || c == '\t')) {
			++r;
			c = *r;
		}

		return r;
	}

	static int __INISESSION(char * src, std::string & out) {
		char * p = src;
		bool quote = false;

		while (*p) {
			char ch = *p;

			if (quote) {
				if (ch == '"') {
					if (out.empty()) return State::SessionSyntax;

					p = __INISKIP(p + 1);
					if (!*p) return State::BraceMismatch;
					if (*p != ']') return State::SessionSyntax;

					p = __INISKIP(p + 1);
					if (*p && *p != '#' && *p != ';') return State::SessionSyntax;

					return 0;
				} else {
					out.push_back(ch);
				}
			} else if (ch == '"') {
				if (!out.empty()) return State::SessionSyntax;
				quote = true;
			} else if (ch == '#' || ch == ';' || ch == '[' || ch == '=') {
				return State::SessionSyntax;
			} else if (ch == ' ' || ch == '\t') {
				p = __INISKIP(p + 1);
				if (*p && *p != ']') return State::SessionSyntax;
				continue;
			} else if (ch == ']') {
				if (out.empty()) return State::LostSession;

				p = __INISKIP(p + 1);
				if (*p && *p != '#' && *p != ';') return State::SessionSyntax;
				return 0;
			} else {
				out.push_back(ch);
			}

			++p;
		}

		return State::SessionSyntax;
	}

	static int __INIKEY(char ** src, std::string & out) {
		char * p = *src;
		bool quote = false;

		while (*p) {
			char ch = *p;

			if (quote) {
				if (ch == '"') {
					if (out.empty()) return State::KeySyntax;

					p = __INISKIP(p + 1);
					if (!*p) return State::EqualMismatch;
					if (*p != '=') return State::KeySyntax;

					*src = p + 1;
					return 0;
				} else {
					out.push_back(ch);
				}
			} else if (ch == '"') {
				if (!out.empty()) return State::KeySyntax;
				quote = true;
			} else if (ch == '#' || ch == ';' || ch == '[' || ch == ']') {
				return State::KeySyntax;
			} else if (ch == ' ' || ch == '\t') {
				p = __INISKIP(p + 1);
				if (*p && *p != '=') return State::KeySyntax;
				continue;
			} else if (ch == '=') {
				if (out.empty()) return State::LostKey;
				*src = p + 1;
				return 0;
			} else {
				out.push_back(ch);
			}

			++p;
		}

		return State::KeySyntax;
	}

	static int __INIVALUE(char * src, std::string & out) {
		char * p = src;
		bool quote = false;

		while (*p) {
			char ch = *p;

			if (quote) {
				if (ch == '"') {
					p = __INISKIP(p + 1);
					if (*p && *p != '#' && *p != ';') return State::ValueSyntax;
					return 0;
				} else {
					out.push_back(ch);
				}
			} else if (ch == '"') {
				if (!out.empty()) return State::ValueSyntax;
				quote = true;
			} else if (ch == '[' || ch == ']' || ch == '=') {
				return State::ValueSyntax;
			} else if (ch == '#' || ch == ';') {
				return 0;
			} else if (ch == ' ' || ch == '\t') {
				p = __INISKIP(p + 1);
				if (*p && *p != '#' && *p != ';') return State::ValueSyntax;
				return 0;
			} else {
				out.push_back(ch);
			}

			++p;
		}

		if (quote) return State::QuoteMismatch;
		return 0;
	}
}

bool Ini::Load(const std::string & path) {
	_error = 0;

	if (path.empty()) {
		_error = State::Param;
		return false;
	}

	FILE * file = NULL;

#if defined(_WIN32)
	if (fopen_s(&file, path.c_str(), "r") != 0) {
#else
	if ((file = fopen(path.c_str(), "r")) == NULL) {
#endif
		_error = State::FileAccess;
		return false;
	}

    struct Guard {
        FILE * p;
        Guard(FILE * file) : p(file) {}
        ~Guard() { if (p) fclose(p); }
    } _(file);

	/// Skip UTF-8 bomb
	{
		uint8_t bomb[3] = { 0, 0, 0 };
		size_t count = fread(bomb, 1, 3, file);
		if (count < 3 || !(bomb[0] == 0xEF && bomb[1] == 0xBB && bomb[2] == 0xBF)) fseek(file, 0, SEEK_SET);
	}	

	char buf[2048];
	std::string line, session, key, value;	

	while (!feof(file)) {
		::memset(buf, 0, 2048);
		fgets(buf, 2048, file);

		line = __INITRIM(buf, " \t\r\n");
		if (line.empty()) continue;

		::memset(buf, 0, 2048);
		::memcpy(buf, line.c_str(), line.length());

		char ch = *buf;
		char * p = buf;

		if (ch == '#' || ch == ';') {
			continue;
		} else if (ch == '[') {
			++p;
			session.clear();
			if ((_error = __INISESSION(p, session)) != 0) return false;
		} else if (session.empty()) {
			_error = State::LostSession;
			return false;
		} else {
			key.clear();
			value.clear();

			if ((_error = __INIKEY(&p, key)) != 0) return false;
			p = __INISKIP(p);

			if ((_error = __INIVALUE(p, value)) != 0) return false;
			if (value.empty()) continue;
			
			auto it = _sessions.find(session);
			if (it == _sessions.end()) {
				std::map<std::string, std::string> kv;
				kv[key] = value;
				_sessions[session] = kv;
			} else {
				it->second[key] = value;
			}
		}
	}

	return true;
}

std::string Ini::GetError() {
	switch (_error) {
	case State::Success:
		return "Success";
	case State::Param:
		return "Param for Ini::Load error";
	case State::FileAccess:
		return "Can NOT access gaven file";
	case State::LostSession:
		return "Lost session";
	case State::LostKey:
		return "Lost key";
	case State::SessionSyntax:
		return "Session syntax error";
	case State::KeySyntax:
		return "Key syntax error";
	case State::ValueSyntax:
		return "Value syntax error";
	case State::BraceMismatch:
		return "Brace mismatch";
	case State::QuoteMismatch:
		return "Quote mismatch";
	case State::EqualMismatch:
		return "Equal mismatch";
	default:
		return "Unknown error";
	}
}

std::vector<std::string> Ini::GetSessions() {
	std::vector<std::string> ret;
	for (auto it = _sessions.begin(); it != _sessions.end(); ++it)
		ret.push_back(it->first);
	return std::move(ret);
}

std::vector<std::string> Ini::GetKeys(const std::string & sSession) {
	std::vector<std::string> ret;
	
	auto s = _sessions.find(sSession);
	if (s == _sessions.end()) return std::move(ret);

	for (auto k = s->second.begin(); k != s->second.end(); ++k)
		ret.push_back(k->first);

	return std::move(ret);
}

std::string Ini::Get(const std::string & session, const std::string & key, const std::string & def) const {
    auto s = _sessions.find(session);
    if (s == _sessions.end()) return def;

    auto k = s->second.find(key);
    if (k == s->second.end()) return def;

    return k->second;
}

void Ini::Push2Lua(struct lua_State * lua) {
	lua_newtable(lua);

	auto sessions = GetSessions();
	for (auto & s : sessions) {
		lua_newtable(lua);
		
		auto keys = GetKeys(s);
		for (auto & k : keys) {
			auto val = Get(s, k, "");
			if (!val.empty()) {
				lua_pushlstring(lua, val.c_str(), val.size());
				lua_setfield(lua, -2, k.c_str());
			}
		}
		lua_setfield(lua, -2, s.c_str());
	}
}