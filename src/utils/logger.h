#ifndef     __OMNI_LOGGER_H_INCLUDED__
#define     __OMNI_LOGGER_H_INCLUDED__

#include    <cstdio>
#include    <mutex>

/**
 * Log tools.
 */
class Logger {
public:
    Logger();
    ~Logger();

    /** Get runtime singleton instance of Logger */
    static Logger & Instance();

    /** Enable verbose log message */
    inline void EnableVerbose() { _enable_verbose = true; }

    /** Write log message interfaces. */
    void VerboseV(const char * fmt, va_list args);
    void Verbose(const char * fmt, ...);
    void Info(const char * fmt, ...);
    void Warn(const char * fmt, ...);
    void Error(const char * fmt, ...);

private:
    void __Write(char type, const char * fmt, va_list args);

private:
    int _cur_day;
    bool _enable_verbose;
    FILE * _file;
    char * _buf;
    std::mutex _lock;
};

#endif//!   __OMNI_LOGGER_H_INCLUDED__