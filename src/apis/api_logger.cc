#include    "../utils/logger.h"

extern "C" {
#include	<luajit.h>
#include	<lualib.h>
#include	<lauxlib.h>
}

namespace {

    static int Error(lua_State * lua) {
		const char * msg = lua_tostring(lua, 1);
		if (!msg) return 0;
		Logger::Instance().Error(msg);
		return 0;
	}

	static int Warn(lua_State * lua) {
		const char * msg = lua_tostring(lua, 1);
		if (!msg) return 0;
		Logger::Instance().Warn(msg);
		return 0;
	}

	static int Info(lua_State * lua) {
		const char * msg = lua_tostring(lua, 1);
		if (!msg) return 0;
		Logger::Instance().Info(msg);
		return 0;
	}

	static int Verbose(lua_State * lua) {
		const char * msg = lua_tostring(lua, 1);
		if (!msg) return 0;
		Logger::Instance().Verbose(msg);
		return 0;
	}

}

void RegisterApi_Logger(lua_State * lua) {
	lua_newtable(lua);

	lua_pushcfunction(lua, &Error);
	lua_setfield(lua, -2, "error");
	lua_pushcfunction(lua, &Warn);
	lua_setfield(lua, -2, "warn");
	lua_pushcfunction(lua, &Info);
	lua_setfield(lua, -2, "info");
	lua_pushcfunction(lua, &Verbose);
	lua_setfield(lua, -2, "verbose");

	lua_setglobal(lua, "log");
}