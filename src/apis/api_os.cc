#include	"../utils/os.h"

extern "C" {
#include	<luajit.h>
#include	<lualib.h>
#include	<lauxlib.h>
}

namespace {
    static int Exists(lua_State * lua) {
        lua_pushboolean(lua, ::Exists(lua_tostring(lua, 1)));
        return 1;
    }

    static int MakeDir(lua_State * lua) {
        lua_pushboolean(lua, ::MakeDir(lua_tostring(lua, 1)));
        return 1;
    }

    static int Copy(lua_State * lua) {
        const char * path = lua_tostring(lua, 1);
        const char * to = lua_tostring(lua, 2);
        lua_pushboolean(lua, ::Copy(path, to));
        return 1;
    }

    static int Delete(lua_State * lua) {
        lua_pushboolean(lua, ::Delete(lua_tostring(lua, 1)));
        return 1;
    }

    static int FileSize(lua_State * lua) {
        lua_pushinteger(lua, (lua_Integer)::FileSize(lua_tostring(lua, 1)));
        return 1;
    }
}

void RegisterApi_OS(lua_State * lua) {
    lua_getglobal(lua, "os");

    lua_pushcfunction(lua, &Exists);
    lua_setfield(lua, -2, "exists");

    lua_pushcfunction(lua, &MakeDir);
    lua_setfield(lua, -2, "mkdir");

    lua_pushcfunction(lua, &Copy);
    lua_setfield(lua, -2, "cp");

    lua_pushcfunction(lua, &Delete);
    lua_setfield(lua, -2, "rm");

    lua_pushcfunction(lua, &FileSize);
    lua_setfield(lua, -2, "filesize");

    lua_pop(lua, 1);
}
