#ifndef     __OMNI_ERRORPAGE_H_INCLUDED__
#define     __OMNI_ERRORPAGE_H_INCLUDED__

#include    <map>
#include    <string>

/**
 * Static error page content
 */
class ErrorPage {
public:
    ErrorPage() {}

    /**
     * Get runtime singleton instance of ErrorPage
     */
    static ErrorPage & Instance();

    /**
     * Set page for HTTP error
     * 
     * \param   err_code    HTTP error code.
     * \param   page        Static HTML page path.
     */
    void Set(int err_code, const std::string & page);

    /**
     * Get error page
     * 
     * \param   err_code    HTTP error code.
     * \return HTML content
     */ 
    const std::string & Get(int err_code);

private:
    std::map<int, std::string> _pages;
};

#endif//!   __OMNI_ERRORPAGE_H_INCLUDED__