#ifndef     __OMNI_RESPONSE_H_INCLUDED__
#define     __OMNI_RESPONSE_H_INCLUDED__

#include    <map>
#include    <string>
#include    <vector>

/**
 * Response helper
 */
class Response {
    typedef std::map<std::string, std::string> Headers;
    typedef std::vector<std::string> Cookies;

public:
    Response(struct MHD_Connection * conn);

    /**
     * Prepare response for Lua
     * 
     * \param   lua     Lua VM
     */
    void Prepare(struct lua_State * lua);

    /**
     * Set http header
     * 
     * \param   key     Key for set header
     * \param   val     Header value.
     */
    void Header(const std::string & key, const std::string & val) { _headers[key] = val; }

    /**
     * Add Set-Cookie request to response.
     * 
     * \param   cookie  Cookie data.
     */
    void Cookie(const std::string & cookie) { _cookies.push_back(cookie); }

    /**
     * Write data into response body
     * 
     * \param   data    Data to write
     */
    void Echo(const std::string & data) { _body.append(data); }

    /**
     * Send response to client immediately
     */
    void Flush();

    /**
     * Send error to client
     * 
     * \param   http_code   Error code
     */
    void Error(int http_code);

    /**
     * Send redirect location request to client
     * 
     * \param   url     Redirect location.
     */
    void Redirect(const std::string & url);

    /**
     * Serve file request.
     * 
     * \param   url    Request url
     */
    void File(const std::string & url);

private:
    struct MHD_Connection * _conn;
    int _http_code;
    Headers _headers;
    Cookies _cookies;
    std::string _body;
};

#endif//!   __OMNI_RESPONSE_H_INCLUDED__