#ifndef     __OMNI_STORAGE_H_INCLUDED__
#define     __OMNI_STORAGE_H_INCLUDED__

#include    <map>
#include    <mutex>
#include    <string>

/**
 * Container to store global values includes session data.
 */
class Storage {
public:
    Storage();
    ~Storage();

    /** Get runtime singleton instance of Storage. */
    static Storage & Instance();

    /**
     * Set data expire time.
     * 
     * \param   life_time   Data's life time in milliseconds.
     */
    inline void SetExpire(double life_time) { _expire = life_time; }

    /**
     * Get data expire time.
     */
    inline double GetExpire() const { return _expire; }

    /**
     * Try to get data stored by key
     * 
     * \param   key Key to find this data.
     * \param   out Data content if found.
     * \return Is this data exists?
     */
    bool TryGet(const std::string & key, std::string & out);

    /**
     * Store data
     * 
     * \param   key             Key to find this data.
     * \param   val             Data's content.
     * \param   never_expire    Should this data never expired?
     */
    void Set(const std::string & key, const std::string & val, bool never_expire = false);

private:
    double _expire;
    std::mutex _lock;
    std::map<std::string, struct StorageData *> _data;
};

#endif//!   __OMNI_STORAGE_H_INCLUDED__