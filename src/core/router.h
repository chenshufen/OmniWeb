#ifndef     __OMNI_ROUTER_H_INCLUDED__
#define     __OMNI_ROUTER_H_INCLUDED__

#include    <string>
#include    <vector>
#include    <map>

/**
 * Simple router using Regex.
 */
class Router {
public:
    Router() : _rules() {}
    virtual ~Router();

    /**
     * Prepare router for Lua
     * 
     * \param   lua     Lua VM
     */
    void Prepare(struct lua_State * lua);

    /**
     * Interface for register router rules.
     * 
     * \param   pattern     Match pattern. Regex expr supported.
     * \param   lua_proc    Handler function reference.
     */
    inline void Any(const std::string & pattern, int lua_proc) { __AddRule("ANY", pattern, lua_proc); }
    inline void Get(const std::string & pattern, int lua_proc) { __AddRule("GET", pattern, lua_proc); }
    inline void Post(const std::string & pattern, int lua_proc) { __AddRule("POST", pattern, lua_proc); }

    /**
     * Check if request is allowed
     * 
     * \param   url     URL
     * \param   method  HTTP method
     * \param   proc    If allowed returns lua function reference.
     * \param   matches Sub metches of rule.
     * \return HTTP code.
     */
    int Check(const std::string & url, const std::string & method, int & proc, std::vector<std::string> & matches) const;

private:
    void __AddRule(const std::string & method, const std::string & pattern, int lua_proc);

private:
    std::vector<struct Rule *> _rules;
};

#endif//!   __OMNI_ROUTER_H_INCLUDED__