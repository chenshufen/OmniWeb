#ifndef     __OMNI_REQUEST_H_INCLUDED__
#define     __OMNI_REQUEST_H_INCLUDED__

#include    <string>
#include    <map>
#include    <vector>
#include    <microhttpd.h>

extern "C" {
#include    <luajit.h>
#include    <lualib.h>
#include    <lauxlib.h>
}

/**
 * HTTP request parameter
 */
namespace Request {
    
    /**
     * Post data
     */
    struct PostData {
        MHD_PostProcessor * pp;
        double start_time;
        FILE * file;
        std::string file_name;
        std::map<std::string, std::string> params;
        std::map<std::string, std::string> uploaded;
        std::map<std::string, std::vector<std::string>> arrays;
    };

    /**
     * Prepare request data for Lua
     *
     * \param   conn        Request client
     * \param   lua         Lua VM
     * \param   start_time  Real start time
     * \param   url         Request url
     * \param   method      HTTP method
     * \param   upload_data Upload data for POST processing
     * \param   upload_size Upload data size for POST processing
     * \param   conn_data   User data for post processing.
     * \reutrn Is request processing done?
     */
    extern bool Prepare(
        MHD_Connection * conn,
        lua_State * lua,
        double & start_time,
        const std::string & url,
        const std::string & method,
        const char * upload_data,
        size_t * upload_size,
        void ** conn_data);
}

#endif//!   __OMNI_REQUEST_H_INCLUDED__