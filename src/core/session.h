#ifndef     __OMNI_SESSION_H_INCLUDED__
#define     __OMNI_SESSION_H_INCLUDED__

#include    <string>

extern "C" {
#include    <luajit.h>
#include    <lualib.h>
#include    <lauxlib.h>
}

/**
 * Session data
 */
class Session {
public:
    Session(lua_State * lua, struct MHD_Connection * conn, class Response * rsp);
    virtual ~Session();

    /**
     * Initialize session module.
     * 
     * \param enabled Enabled/Disabled.
     */
    static void Init(bool enabled);

private:
    lua_State * _vm;
    std::string _id;
};

#endif//!   __OMNI_SESSION_H_INCLUDED__