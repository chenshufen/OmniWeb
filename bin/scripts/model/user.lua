-- ========================================================
-- @File    : /scripts/model/user.lua
-- @Brief   : 一个简单的Model实例
-- @Author  : Leo Zhao
-- @Date    : 2018-08-03
-- ========================================================
local user = inherit(M('base')); -- 声明一个model

-- 查找所有的用户
function user:all()
    return self:query[[SELECT * FROM `users`]];
end

-- 根据ID找到指定用户
function user:get(id)
    return self:query([[SELECT * FROM `users` WHERE `id`=?1]], id)[1]; -- query返回的是数组，这里因为id唯一所以直接取第一个目标
end